<?php
  session_start();
  include 'funcions.php';
  include 'comprovar_usuari.php';

  // Elimina els tuits
  if (isset($_GET['delIdTuit'])) {
    if (delMissatge($_GET['delIdTuit'])) {
      setcookie("error_login", '');
      header("location:inici.php");
    } else {
      $value = "No s'ha pogut eliminar el tuit";
      setcookie("error_login", $value);
      header("location:inici.php");
    }
  }

  // Publica tuits
  if(isset($_POST['comment'])) {
    if(afegirMissatge($_SESSION['usuari'], $_POST['comment'])){
      header("location:inici.php");
      setcookie("error_login", '');
    } else {
      $value = "No s'ha pogut afegir el tweet";
      setcookie("error_login", $value);
      header("location:inici.php");
    }
  }

  // Fica likes a als tuits
  if(isset($_GET['likeIdTuit'])) {
    if (addLikeMissatge($_GET['likeIdTuit'])) {
      setcookie("error_login", '');
      header("location:inici.php");
    } else {
      $value = "No s'ha pogut fer el like";
      setcookie("error_login", $value);
      header("location:inici.php");
    }
  }

  // Fa follow si es pot
  if (isset($_GET['followUserId'])) {
    if(followUser($_SESSION['usuari'], $_GET['followUserId'])) {
      setcookie("error_login", '');
      header("location:inici.php");
    } else {
      $value = "No pots seguir aquest usuari";
      setcookie("error_login", $value);
      header("location:inici.php");
    }
  }

  // Fa unfollow
  if (isset($_GET['unfollowUserId'])) {
    unFollowUser($_SESSION['usuari'], $_GET['unfollowUserId']);
    setcookie("error_login", '');
    header("location:inici.php");
  }

?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>MiniTwitter Inici</title>
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
    <?php
      echo "<h1 id='welcome'>Benvingut al MiniTwitter " . $_SESSION['usuari'] . "</h1>";
      if(isset($_COOKIE['error_login'])){
        echo '<p>' . $_COOKIE["error_login"] . '</p>';
      }
    ?>
    <br>
    <div id="content">
      <!-- Div per mostrar i afegir tweets del propi usuari -->
      <div id="ownUser">
        <h2>Els teus tuits</h2>
        <?php
          echo "<table width=100%>";
          // Recupera els missatges de l'usuari i els escriu
          getMissatges($_SESSION['usuari'], true);
          echo "</table>";
        ?>
        <br>
        Publicar un nou tweet:<br>
        <textarea rows="4" cols="50" name="comment" form="twitter"></textarea><br>
        <form method="post" id="twitter">
          <br>
          <button id="enviar" class="button2">Publicar</button>
          <a href="sortir.php"><button  class="button1" type="button" name="sortir">Sortir</button></a>
        </form>
        <br>
        <table width="50%">
          <tr>
            <th>Altres usuaris del Mini Twitter</th>
          </tr>
          <?php
            // Llista tots els usuaris del twitter menys el de la sessió
            listFollowUsers($_SESSION['usuari']);
          ?>
        </table>
        <br><br>
      </div>
      <!-- Div per els tweets d'altres usuaris -->
      <div id="otherUser">
        <h2>Història</h2>
        <?php
          // Taula amb els tuits del usuaris que es segueixen
          echo "<table width=100% id='historia'>";
            mostrarTuitsHistoria($_SESSION['usuari']);
          echo "</table>";
          // Serveix per comprovar si s'ha triat un usuari amb el selector
          // aixo fa que cada cop que es carregui la pàgina no es perdi
          // els tuits de l'usuari seleccionat. $_SESSION['otherUser']
          if(isset($_POST['selUser']) || $_SESSION['otherUserSelected'] != "") {
            if (isset($_POST['selUser'])) {
              $_SESSION['otherUserSelected'] = $_POST['selUser'];
            }
            echo "<table width=100% id='selectUser'>
            <tr>
              <th>Tweets usuari triat " . $_SESSION['otherUserSelected'] ."</th>
            </tr>";
            // Escriu els missatges de l'usuari que s'ha triat
            getMissatges($_SESSION['otherUserSelected'], false);
            echo "</table>";
          }
        ?>
        <!-- Formulari per canviar d'usuari -->
        <br>
        <form method="post" id="canvi">
          Triar usuari:
          <select name="selUser">
            <option value=""></option>
            <?php
              getUsuaris($_SESSION['usuari']);
            ?>
          </select>
          <button type="submit" class="button" name="canviar">Canviar</button>
        </form>
        <br><br>
      </div>
    </div>
  </body>
</html>


<!-- Quan vens de l'inici per sig in no es crea la variable
 i despres dona un error -->
