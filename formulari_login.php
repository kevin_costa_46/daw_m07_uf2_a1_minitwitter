<?php
  session_start();
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Login</title>
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
    <h1>Login</h1>
    <?php
      if(isset($_COOKIE['error_login'])){
        echo '<p>' . $_COOKIE["error_login"] . '</p>';
      }
    ?>
    <form action="processa_login.php" method="post">
      Username:<br>
      <input type="text" name="username"><br>
      Password:<br>
      <input type="password" name="passwd"><br><br>
      <button type="submit" id="submit">Login</button>
      <a href="formulari_signin.php"><button type="button" name="signin">Signin</button></a>
    </form>
  </body>
</html>
