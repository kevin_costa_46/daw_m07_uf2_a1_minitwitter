<?php
  session_start();
  include "funcions.php";
  $USER = $_POST['username'];
  $PASS = $_POST['passwd'];

  if(!comprovarUsuari($USER, $PASS)) {
    $value = "Error login";
    setcookie("error_login", $value);
    header("location:formulari_login.php");
  } else {
    $_SESSION['usuari'] = $USER;
    $_SESSION['otherUserSelected'] = "";
    setcookie("error_login", '');
    header("location:inici.php");
  }
?>
