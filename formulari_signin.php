<?php
  session_start();
  include 'funcions.php';

  if(isset($_POST['name']) && isset($_POST['username']) && isset($_POST['passwd'])) {
    $name = $_POST['name'];
    $username = $_POST['username'];
    $passwd = $_POST['passwd'];
    // Comprova si existeix l'usuari
    if(comprovarUsuari($username, $passwd)) {
      $value = "Ja existeix l'usuari";
      setcookie("error_login", $value);
      header("location:formulari_signin.php");
    } else {
      // Comprava si s'ha pogut afegir l'usuari
      if(afegirUsuari($name, $username, $passwd)){
        $_SESSION['usuari'] = $username;
        $_SESSION['otherUserSelected'] = "";
        setcookie("error_login", '');
        header("location:inici.php");
      } else {
        $value = "No s'ha pogut crear l'usuari";
        setcookie("error_login", $value);
        header("location:formulari_signin.php");
      }
    }
  }
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Sign up</title>
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
    <h1>Sign up</h1>
    <?php
      if(isset($_COOKIE['error_login'])){
        echo '<p>' . $_COOKIE["error_login"] . '</p>';
      }
    ?>
    <form name="signup" accept-charset="utf-8" method="post">
      Name:<br>
      <input type="text" name="name"><br>
      Username:<br>
      <input type="text" name="username"><br>
      Password:<br>
      <input type="password" name="passwd"><br><br>
      <button type="submit" id="submit">Sign up</button>
      <a href="formulari_login.php"><button type="button" name="login">Login</button></a>
    </form>
  </body>
</html>
