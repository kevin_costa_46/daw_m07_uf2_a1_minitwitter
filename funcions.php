<?php

  function connectar() {
    $hostname = "localhost";
    $dbname = "minitwitter";
    $username = "kevin";
    $pw = "admin";
    $pdo = new PDO ("mysql:host=$hostname;dbname=$dbname","$username","$pw");
    return $pdo;
  }

  // Comprova que l'usuari i contrasenya siguin correctes
  function comprovarUsuari($user, $passwd) {
    try {
      $pdo = connectar();
      $sql = "select count(*) as n from users where user = ? and password = MD5(?)";
      $query = $pdo->prepare($sql);
      $query->execute(array($user, $passwd));
      $resultat= $query->fetch();
      if ($resultat['n'] == 0 ) {
        //Si no existeix l'usuari
        return false;
      } else {
        //Si existeix l'usuari
        return true;
      }
    } catch (Exception $e) {
      echo "Error consulta" . $e->getMissatges() . "\n";
      return false;
    }
  }

  // Funció per afegir usuaris
  function afegirUsuari($name, $username, $passwd) {
    try {
      $pdo = connectar();
      $sql = $pdo->prepare("insert into users (name, user, password) values(?,?,MD5(?))");
      $sql->execute(array($name, $username, $passwd));
      return true;
    } catch (Exception $e) {
      echo "Error consulta" . $e->getMissatges() . "\n";
      return false;
    }
  }

  // Funció per obtenir tots els usuaris de base de dades
  function getUsuaris($value) {
    try {
      $pdo = connectar();
      $sql = $pdo->prepare("select * from users");
      $sql->execute();
      foreach ($sql as $row) {
        // Si es el mateix usuari de la sessio no el fica
        if (!($row['user'] == $value)) {
          // Amb cada usuari que troba fa una option nova
          echo '<option value="' . $row['user'] . '">' . $row['user'] . '</option>';
        }
      }
    } catch (Exception $e) {
      echo "Error consulta" . $e->getMessage() . "\n";
    }
  }

  // Funció per ficar missatges en una taula. $buttonDel=true/false
  // serveix per dir si es vol ficar el button d'eliminar o no
  function getMissatges($userId, $buttonDel) {
    try {
      $pdo = connectar();
      // Agafa tots el tuits i els ordena
      $sql = $pdo->prepare("select * from tweets where usersId = ? order by date desc");
      $sql->execute(array($userId));
      // Per cada tuit que ha trobat
      foreach ($sql as $row) {
        echo "<tr>";
        // Comprova si ha de ficar el <button> d'eliminar
        if ($buttonDel) {
          echo "<td id=".$row['id'] . ">" . $row['text'] . " <br />
          Likes - " . $row['megusta'] . "
          <a href='inici.php?delIdTuit=". $row['id'] ."'><button class='button1'>Eliminar</button></a>
          <a href='inici.php?likeIdTuit=". $row['id'] ."'><button class='button3'>M'agrada</button></a>
          </td>";
        } else {
          echo "<td id=".$row['id'] . ">" . $row['text'] . "<br />
          Likes - " . $row['megusta'] . "
          <a href='inici.php?likeIdTuit=". $row['id'] ."'><button class='button3'>M'agrada</button></a>
          </td>";
        }
        echo "</tr>";
      }
    } catch (Exception $e) {
      echo "Error consulta" . $e->getMessage() . "\n";
    }
  }

  // Funció per afegir missatges
  function afegirMissatge($userId, $text) {
    try {
      $pdo = connectar();
      $fecha = date("Y-m-d H:i:s");
      $sql = $pdo->prepare("insert into tweets (text, date, usersId) values(?,?,?)");
      $sql->execute(array($text, $fecha, $userId));
      return true;
    } catch (Exception $e) {
      echo "Error consulta" . $e->getMissatges() . "\n";
      return false;
    }
  }

  // Funció per eliminar missatges per id
  function delMissatge($idMiss) {
    try {
      $pdo = connectar();
      $sql = $pdo->prepare("delete from `tweets` WHERE id = ?");
      $sql->execute(array($idMiss));
      return true;
    } catch (Exception $e) {
      echo "Error consulta" . $e->getMissatges() . "\n";
      return false;
    }
  }

  // Funció per ficar likes als tuits
  // li suma un 1 al número de likes que te
  function addLikeMissatge($idMiss) {
    try {
      $pdo = connectar();
      $quantLikes = numberLikesMissatge($idMiss);
      $quantLikes++;
      $sql = $pdo->prepare("update tweets set megusta = ? WHERE id = ?");
      $sql->execute(array($quantLikes, $idMiss));
      return true;
    } catch (Exception $e) {
      echo "Error consulta" . $e->getMissatges() . "\n";
      return false;
    }
  }

  // Afagar els likes que ja te un tuit
  function numberLikesMissatge($idMiss) {
    try {
      $pdo = connectar();
      $sql = $pdo->prepare("select megusta from tweets where id = ?");
      $sql->execute(array($idMiss));
      $r = $sql->fetch();
      $resultat = $r['megusta'];
      return $resultat;
    } catch (Exception $e) {
      echo "Error consulta" . $e->getMissatges() . "\n";
      return false;
    }
  }

  ////////////////////////////////
  // Funcions per els followers //
  ////////////////////////////////

  // Funció per fer una llista amb usuaris per seguir
  function listFollowUsers($value) {
    try {
      $pdo = connectar();
      $sql = $pdo->prepare("select * from users");
      $sql->execute();
      foreach ($sql as $row) {
        if (!($row['user'] == $value)) {
          echo "<tr>
          <td>" . $row['user'] .
          "<a href='inici.php?unfollowUserId=" . $row['user'] . "'><button class='button1'>Unfollow</button></a>
          <a href='inici.php?followUserId=" . $row['user'] . "'><button class='button2'>Follow</button></a>
          </td>
          </tr>";
        }
      }
    } catch (Exception $e) {
      echo "Error consulta" . $e->getMessage() . "\n";
    }
  }

  // Funció per seguir a un usuari
  function followUser($idUser, $idFollowUser) {
    try {
      if (isFollowed($idUser, $idFollowUser)) {
        $pdo = connectar();
        $sql = $pdo->prepare("insert into followers(user_id, follower_id) values(?,?)");
        $sql->execute(array($idUser,$idFollowUser));
        return true;
      } else {
        return false;
      }
    } catch (Exception $e) {
      echo "Error consulta" . $e->getMessage() . "\n";
    }
  }

  // Funció per deixar de seguir a un usuari
  function unFollowUser($idUser, $idFollowUser) {
    try {
      $pdo = connectar();
      $sql = $pdo->prepare("delete from `followers` where user_id=? and follower_id=?");
      $sql->execute(array($idUser,$idFollowUser));
    } catch (Exception $e) {
      echo "Error consulta" . $e->getMessage() . "\n";
    }
  }

  // Funció per comprovar si ja s'esta seguint a un usuari
  function isFollowed($idUser, $idFollowUser) {
    try {
      $pdo = connectar();
      $sql = $pdo->prepare("select count(*) as n from followers where user_id=? and follower_id=?");
      $sql->execute(array($idUser,$idFollowUser));
      $resultat = $sql->fetch();
      if ($resultat['n'] == 0) {
        return true;
      } else {
        return false;
      }
    } catch (Exception $e) {
      echo "Error consulta" . $e->getMessage() . "\n";
    }
  }

  // Funció per mostrar els tuits a la historia
  // fent servir la taula followers que diu a qui segueix
  function mostrarTuitsHistoria($idUser) {
    try {
      $pdo = connectar();
      // Agafa tots els ids dels que segueix
      $sql = $pdo->prepare("select follower_id from followers where user_id=?");
      $sql->execute(array($idUser));
      // Per cada id de seguidor que segueix
      foreach ($sql as $row ) {
        // Fa una select i agafa els seus tuits
        $sql2 = $pdo->prepare("select * from tweets where usersId=? order by date desc");
        $sql2->execute(array($row['follower_id']));
        // Per cada tuit d'usuari que segueix
        foreach ($sql2 as $row2) {
          // Fa un <tr> i el fica a la taula
          echo "<tr>";
            echo "<td id=".$row2['id'].">". $row2['text'] ." <br/>
            Likes - " . $row2['megusta'] . " - Usuari: " . $row2['usersId'] ."
            <a href='inici.php?likeIdTuit=". $row2['id'] ."'>
            <button class='button3'>M'agrada</button></a>
            </td>";
          echo "</tr>";
        }
      }
    } catch (Exception $e) {
      echo "Error consulta" . $e->getMessage() . "\n";
    }
  }
?>
